package org.privacyhelper.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.privacyhelper.FragmentIds;
import org.privacyhelper.R;
import org.privacyhelper.activities.DangerousPermissionsActivity;
import org.privacyhelper.activities.OtherResourcesActivity;
import org.privacyhelper.activities.RecommendedAppsActivity;
import org.privacyhelper.activities.TechniquesActivity;
import org.privacyhelper.activities.TextActivity;
import org.privacyhelper.adapters.ClickListener;
import org.privacyhelper.adapters.RAdapter;

/**
 * LearnFragmentTab
 * The fragment that displays all of the different guides in the app
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class LearnFragmentTab extends Fragment {

    RecyclerView mRecyclerView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //get the view
        View rootView = inflater.inflate(R.layout.activity_recycler_view, container, false);

        //get the recycler view
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rView);
        setRecyclerViewOptions();


        //create a string array of titles to display
        String[] itemTitles = new String[]{getString(R.string.privacy_basics_text), getString(R.string.app_privacy_overview_text), getString(R.string.permissions_demo_text),
                getString(R.string.prevention_techniques_text), getString(R.string.recommended_apps_text), getString(R.string.useful_resources_activity_text)};

        //specify the adapter for the current view
        RAdapter mAdapter = new RAdapter(itemTitles, R.id.itemConstraintLayout);
        mRecyclerView.setAdapter(mAdapter);

        //Disable swipe to refresh
        rootView.findViewById(R.id.swipe_container).setEnabled(false);

        //Add action to recyclerview item click
        mAdapter.setClickListener(new ClickListener() {
            @Override
            public void itemClicked(View view, int position) {
                launchMenuItem(position);   //call the method handling intents
            }
        });

        return rootView;
    }

    /*Initializes recyclerview properties*/
    private void setRecyclerViewOptions() {
        //using recommended setting to improve perf
        mRecyclerView.setHasFixedSize(true);

        //use linear layout
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //Add line decoration to make it more clear that the list has touch interactions
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(),  mLayoutManager.getOrientation()));
    }

    /*Method to handle the intent to provide view switching between info*/
    private void launchMenuItem(int position) {
        Class dest;
        String fragment_id = null;

        switch (position) {
            case 0: //if intro is selected
                dest = TextActivity.class;
                fragment_id = FragmentIds.privacy_basics_fragment_id;
                break;
            case 1: //if basics is selected
                dest = TextActivity.class;
                fragment_id = FragmentIds.privacy_overview_fragment_id;
                break;
            case 2: //if "How am I affected?" is selected
                dest = DangerousPermissionsActivity.class;
                break;
            case 3: //if mitigations is selected
                dest = TechniquesActivity.class;
                break;
            case 4: //if recommended apps is selected
                dest = RecommendedAppsActivity.class;
                break;
            case 5: //if other resources selected
                dest = OtherResourcesActivity.class;
                break;
            default:    //something creepy selected... :(
                Log.e("PrivacyHelper", "Error with Recycler view action handler");
                dest = null;
        }

        //If a destination activity class has
        if (dest != null) {
            if (fragment_id != null)
                startActivity(new Intent(this.getContext(), dest).putExtra(getString(R.string.text_activity_intent_key), fragment_id));
            else
                startActivity(new Intent(this.getContext(), dest));
        }
    }
}
