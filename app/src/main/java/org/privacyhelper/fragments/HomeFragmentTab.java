package org.privacyhelper.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.privacyhelper.R;
import org.privacyhelper.activities.QuizActivity;

import static org.privacyhelper.activities.SettingsActivity.KEY_THEME_PREF_LIST;

/**
 * HomeFragmentTab
 * The main (first) fragment that the user sees upon opening the app
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class HomeFragmentTab extends Fragment {

    // Needed to hold reference to previous result text
    private TextView resultText;
    // Needed to apply app branding image to activity/view
    private ImageView appHomeImage;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_activity_home, container, false);

        //Find button and textview elements
        Button startButton = (Button) rootView.findViewById(R.id.startButton);
        resultText = (TextView) rootView.findViewById(R.id.previousScoreTextView);
        appHomeImage = (ImageView) rootView.findViewById(R.id.privacyImageView);

        //Update the image using resource saving methods
//        loadImageBitmap();

        //Update the image according to the app theme
        applyAppTheme();
        
        //Update the previous result textview
        updatePrevResultText();

        //Add startbutton action
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), QuizActivity.class);
                startActivity(intent);
            }
        });
        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();

        //Update the previous result when activity/view resumes
        updatePrevResultText();
    }

    /*Method to get any previous quiz results*/
    private void updatePrevResultText() {
        //Define error value and get the previous result
        int errorVal = getResources().getInteger(R.integer.default_err_val);
        int previousScore = getPreviousResult();

        /*
        Handle previous score here
         */
        String out = getString(R.string.previous_score_text) + " ";
        if (previousScore != errorVal) {    //previous score exists
            int threshold_val = getResources().getInteger(R.integer.result_thres);
            //Check for all scores
            if (previousScore <= -threshold_val) {  //bad score
                out += getString(R.string.bad_previous_score);
            } else if (previousScore >= threshold_val) {    //good score
                out += getString(R.string.good_previous_score);
            } else {    //okay score
                out += getString(R.string.okay_previous_score);
            }
        }
        else {  //no previous score found
            out += getString(R.string.none_previous_score);
        }
        //Update the text
        resultText.setText(out);
    }

    private int getPreviousResult() {
        /*
        Gets the previous score from sharedPreferences
         */
        SharedPreferences sharedPref = null;
        int errorVal = getResources().getInteger(R.integer.default_err_val);
        int result = errorVal;


        //Attempt to get the shared preference file
        if (this.getContext() != null) {
            //Context found, get shared preferences
            sharedPref = getContext().getSharedPreferences(getString(R.string.results_shared_pref), Context.MODE_PRIVATE);
        } else {
            //No context???
            Log.e("SharedPref", "Context not found");
        }

        //Get the previous score from the shared preference
        if (sharedPref != null) {
            result = sharedPref.getInt(getString(R.string.saved_previous_score_key), errorVal);
        }

        return result;
    }

    /*Applies the app theme to the image element*/
    private void applyAppTheme() {
        //Grab the selection from sharedpreferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        String display_theme = sharedPreferences.getString(KEY_THEME_PREF_LIST, "");

        //Check if night mode
        switch (display_theme) {
            case "Dark":   //is dark theme
                //Set to the white privacy home icon
                appHomeImage.setImageResource(R.drawable.ic_privacy_icon_white);
                break;
            default:    //is light theme
                //Set to the normal (black) privacy home icon
                appHomeImage.setImageResource(R.drawable.ic_privacy_icon);
        }
    }

    /*Method to convert the home image to a bitmap to save resources*/
//    private void loadImageBitmap() {
//        //Define and initialize dimensions and id of image
//        int dimenX = 268, dimenY = 122;
//        int imageId = R.drawable.ic_privacy_icon;
//
//        //Convert to bitmap
//        Bitmap bitmap = BitmapDownscaler.decodeSampledBitmapFromResource(getResources(), imageId, dimenX, dimenY);
//
//        //Set the image to the converted bitmap
//        appHomeImage.setImageBitmap(bitmap);
//    }
}
