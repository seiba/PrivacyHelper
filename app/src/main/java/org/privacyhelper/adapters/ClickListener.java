package org.privacyhelper.adapters;

import android.view.View;

/**
 * ClickListener
 * An interface for supporting click actions specific to recyclerview items
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public interface ClickListener {
    void itemClicked(View view, int position);
}
