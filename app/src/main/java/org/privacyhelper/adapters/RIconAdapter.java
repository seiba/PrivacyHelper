package org.privacyhelper.adapters;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.privacyhelper.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * RIconAdapter
 * Like RAdapter, but also has support for imagecircleview
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class RIconAdapter extends RecyclerView.Adapter<RIconAdapter.ViewHolder> implements Filterable {

    // Set up the variables to hold the Recycler View id and the array to display as items
    private int _id;
    private String[] itemArray = {};    //holds the app text
    private String[] filteredItems = {};    //Filtered array of strings to display
    private Bitmap[] imageIds = {}; //holds the app icons
    private Bitmap[] filteredImageIds = {};  //Filtered array of ids to display
    private ItemFilter mFilter; //The filter object to use
    private int[] filteredIndexes = {}; //holds the filtered item indexes from original array
    private ClickListener clickListener = null; //Holds the reference to the clicklistener

    @Override
    public Filter getFilter() {
        //Check if filter already initialized
        if (mFilter == null) {  //not initialized
            mFilter = new ItemFilter(); //create new instance
        }
        return mFilter;
    }

    /*
    A view holder class to "hold" a view
    Original Author: Pawan Deshpande
    Link: http://tutorialsbuzz.com/2015/12/android-recyclerview-item-click-listener-ripple-effect.html
    Modified by: Joshua Lay
    */
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ConstraintLayout listItem;
        TextView textView;
        CircleImageView circleImageView;

        ViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            listItem = (ConstraintLayout) itemView.findViewById(_id);
            textView = (TextView) itemView.findViewById(R.id.listItemTextView);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.iconCircleView);
        }

        @Override
        public void onClick(View v) {
            //Check first if clicklistener has been assigned
            if (clickListener != null) {
                //Use the clicklistener's method
                clickListener.itemClicked(v, getAdapterPosition());
            }
        }
    }

    /*Sets the clicklistener for this adapter*/
    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    /**
     * Constructor for RIconAdapter
     * @param initTitles the array of strings to display
     * @param initImageIdArray the array of bitmaps (images) to display
     * @param initId the recyclerview id
     */
    public RIconAdapter(String[] initTitles, Bitmap[] initImageIdArray, int initId) {
        //text and image arrays must at least something in them
        if (initTitles.length > 0 && initImageIdArray.length > 0){
            itemArray = initTitles;
            filteredItems = initTitles;
            imageIds = initImageIdArray;
            filteredImageIds = initImageIdArray;
            _id = initId;
        }
    }


    /*A filter class that handles all of the filtering logic for adapter classes*/
    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            //Ensure constraint is valid
            if (constraint != null && constraint.length() > 0) {    //constraint valid
                ArrayList<Integer> indexes = new ArrayList<>();

                //Compare each item with constraint
                for (int i = 0; i < itemArray.length; i++) {
                    if (itemArray[i].toLowerCase()
                            .contains(constraint.toString().toLowerCase())) {
                        //Just add the index
                        indexes.add(i);
                    }
                }
                //copy the items to normal java array
                int[] tmpArray = new int[indexes.size()];
                for (int i = 0; i < indexes.size(); i++) {
                    tmpArray[i] = indexes.get(i);
                }
                //Update results object
                results.count = indexes.size();
                results.values = tmpArray;
            } else {    //not valid
                results = null;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results == null) {  //no result matched, just use full arrays
                filteredIndexes = new int[]{};  //ensure the filtered indexes array is empty
                filteredItems = itemArray;
                filteredImageIds = imageIds;
            } else {    //results obtained
                //Initialize arrays and arraylist
                filteredIndexes = (int[]) results.values;
                String[] names = new String[filteredIndexes.length];
                Bitmap[] bitmap_ids = new Bitmap[filteredIndexes.length];
                //Add the filtered apps to the new arrays
                for (int i = 0; i < filteredIndexes.length; i++) {
                    int index = filteredIndexes[i];
                    names[i] = itemArray[index];
                    bitmap_ids[i] = imageIds[index];
                }
                //Update the filtered arrays
                filteredItems = names;
                filteredImageIds = bitmap_ids;
            }
            //Notify adapter data has changed
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.recycler_list_item_icons, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TextView textView = holder.textView;
        CircleImageView imageView = holder.circleImageView;
        //attempt to set view item resources
        if (position < filteredItems.length && position < filteredImageIds.length) {
            //Only set text/images if position int is appropriate
            textView.setText(filteredItems[position]);
            imageView.setImageBitmap(filteredImageIds[position]);
        } else {
            Log.e("RIconAdapter", "Not enough text items/images to satisfy number of recyclerview items");
        }
    }

    @Override
    public int getItemCount() {
        return filteredItems.length;
    }

    /*Needed so that we can grab the actual index of the filtered item in the original array*/
    public int[] getFilteredIndexes() {
        int[] tmp = new int[filteredIndexes.length];
        System.arraycopy(filteredIndexes, 0, tmp, 0, filteredIndexes.length);
        return tmp;
    }

}





