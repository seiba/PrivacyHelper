package org.privacyhelper.adapters;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.privacyhelper.R;

import java.util.ArrayList;

/**
 * RAdapter
 * The recyclerview adapter that allows the display of text
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class RAdapter extends RecyclerView.Adapter<RAdapter.ViewHolder> implements Filterable {

    // Set up the variables to hold the Recycler View id and the array to display as items
    private int _id;
    private String[] itemArray = {};    //Holds original array of strings to display
    private String[] filteredItems = {};    //Filtered array of strings to display
    private ItemFilter mFilter; //The filter object to use
    private boolean clickable = true;   //Default the recyclerview to present touch ripple feedback
    private ClickListener clickListener = null; //Holds the reference to the clicklistener

    @Override
    public Filter getFilter() {
        //Check if filter already initialized
        if (mFilter == null) {  //not initialized
            mFilter = new ItemFilter(); //create new instance
        }
        return mFilter;
    }

    /*
    A view holder class to "hold" a view
    Original Author: Pawan Deshpande
    Link: http://tutorialsbuzz.com/2015/12/android-recyclerview-item-click-listener-ripple-effect.html
    Modified by: Joshua Lay
    */
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ConstraintLayout listItem;
        TextView textView;

        ViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            listItem = (ConstraintLayout) itemView.findViewById(_id);
            textView = (TextView) itemView.findViewById(R.id.listItemTextView);
        }

        @Override
        public void onClick(View v) {
            //Check first if clicklistener has been assigned and is clickable
            if (clickable && clickListener != null) {
                //Use the clicklistener's method
                clickListener.itemClicked(v, getAdapterPosition());
            }
        }
    }

    /*Sets the clicklistener for this adapter*/
    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    /*A filter class that handles all of the filtering logic for adapter classes*/
    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            //Ensure constraint is valid
            if (constraint != null && constraint.length() > 0) {    //constraint valid
                ArrayList<String> tmp = new ArrayList<>();

                //Compare each item with constraint
                for (String name: itemArray) {
                    if (name.toLowerCase()
                            .contains(constraint.toString().toLowerCase())) {
                        tmp.add(name);
                    }
                }
                //copy the items to normal java array
                String[] tmpArray = new String[tmp.size()];
                for (int i = 0; i < tmp.size(); i++) {
                    tmpArray[i] = tmp.get(i);
                }
                //Update results object
                results.count = tmp.size();
                results.values = tmpArray;
            } else {    //not valid
                // Just return full item list
                results.count = itemArray.length;
                results.values = itemArray;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //Notify adapter data has changed
            filteredItems = (String[]) results.values;
            notifyDataSetChanged();
        }
    }

    /**
     * Constructor for RAdapter
     * @param initTitles the string array of text to put into the list
     * @param initId the recyclerview id to use
     * @author Joshua Lay
     */
    public RAdapter(String[] initTitles, int initId) {
        //String array have something in it
        if (initTitles.length > 0){
            itemArray = initTitles;
            filteredItems = initTitles; //start filtered list with all items
            _id = initId;
        }
    }

    /**
     * Constructor for RAdapter
     * @param initTitles the string array of text to put into the list
     * @param initId the recyclerview id to use
     * @param clickable does the list need touch action
     * @author Joshua Lay
     */
    public RAdapter(String[] initTitles, int initId, boolean clickable) {
        //String array have something in it
        if (initTitles.length > 0){
            itemArray = initTitles;
            filteredItems = initTitles; //start filtered list with all items
            _id = initId;
            this.clickable = clickable;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.recycler_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //If clickable has been defined
        if (!clickable) {
            holder.listItem.setBackgroundColor(0);
        }
        TextView textView = holder.textView;
        textView.setText(filteredItems[position]);
    }

    @Override
    public int getItemCount() {
        return filteredItems.length;
    }

}





