package org.privacyhelper.models;

import android.content.Context;

import org.privacyhelper.R;

import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * PermissionList
 * A holder class that returns a hashmap of permission codes and descriptions
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class PermissionList {

    /*Hashmap to hold known dangerous permission codes and descriptions*/
    private HashMap<String, String> permissionInfo = new HashMap<>();

    /*Needed to support getting app resources*/
    private WeakReference<Context> contextReference;
    
    /**
    * Preferred constructor for PermissionList
    * @param c The contextReference object for getting app resources from
    * @author Joshua Lay
    */
    public PermissionList(Context c) {
        /*We use contextReference here otherwise we can't extract the strings and since this is not an activity class*/
        contextReference = new WeakReference<>(c);
        initPermissionsList();
    }

    /*Initializes the permissionsinfo ArrayList*/
    private void initPermissionsList() {
        Context context = contextReference.get();
        //Insert permission codes and descriptions
        permissionInfo.put("android.permission.ACCESS_FINE_LOCATION", context.getString(R.string.fine_location_access_desc));
        permissionInfo.put("android.permission.CAMERA", context.getString(R.string.camera_access_desc));
        permissionInfo.put("android.permission.BLUETOOTH", context.getString(R.string.bluetooth_access_desc));
        permissionInfo.put("android.permission.READ_CALENDAR", context.getString(R.string.calendar_read_access_desc));
        permissionInfo.put("android.permission.WRITE_CALENDAR", context.getString(R.string.calendar_write_access_desc));
        permissionInfo.put("android.permission.READ_CONTACTS", context.getString(R.string.contacts_read_access_desc));
        permissionInfo.put("android.permission.WRITE_CONTACTS", context.getString(R.string.contacts_write_access_desc));
        permissionInfo.put("android.permission.READ_SMS", context.getString(R.string.sms_read_access_desc));
        permissionInfo.put("android.permission.RECEIVE_SMS", context.getString(R.string.sms_receive_access_desc));
        permissionInfo.put("android.permission.SEND_SMS", context.getString(R.string.sms_send_access_desc));
        permissionInfo.put("android.permission.GET_ACCOUNTS", context.getString(R.string.accounts_access_desc));
        permissionInfo.put("android.permission.RECORD_AUDIO", context.getString(R.string.record_audio_access_desc));
        permissionInfo.put("android.permission.READ_EXTERNAL_STORAGE", context.getString(R.string.external_read_access_desc));
        permissionInfo.put("android.permission.WRITE_EXTERNAL_STORAGE", context.getString(R.string.external_write_access_desc));
        permissionInfo.put("android.permission.READ_CALL_LOG", context.getString(R.string.call_log_read_access));
        permissionInfo.put("android.permission.READ_PHONE_STATE", context.getString(R.string.phone_read_access_desc));
    }

    /*Return the permissions hashmap*/
    public HashMap<String, String> getPermissionInfo() {
        //Return a shallow copy of the hashmap
        return new HashMap<>(permissionInfo);
    }

}
