package org.privacyhelper.models;

import android.content.Context;

import org.privacyhelper.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * AppList
 * A holder class to initialize and return an arraylist of app details
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class AppList {

    /*The arraylist to hold all of the app details*/
    private ArrayList<AppDetails> appDetails = new ArrayList<>();

    /*Needed to support getting app resources*/
    private WeakReference<Context> contextReference;

    /**
     * Preferred constructor for AppList
     * @param c The context object for getting app resources from
     * @author Joshua Lay
     */
    public AppList(Context c) {
        /*We use context here otherwise we can't extract the strings and since this is not an activity class*/
        contextReference = new WeakReference<>(c);
        initAppDetails();
    }

    /*Initializes the appDetails ArrayList*/
    private void initAppDetails() {
        Context context = contextReference.get();
        /*Format: App name, App Package, Bitmap Id*/
        appDetails.add(new AppDetails(context.getString(R.string.facebook_app_name), "com.facebook.katana", R.drawable.facebook_icon));
        appDetails.add(new AppDetails(context.getString(R.string.twitter_app_name), "com.twitter.android", R.drawable.twitter_icon));
        appDetails.add(new AppDetails(context.getString(R.string.instagram_app_name), "com.instagram.android", R.drawable.instagram_icon));
        appDetails.add(new AppDetails(context.getString(R.string.snapchat_app_name), "com.snapchat.android", R.drawable.snapchat_icon));
        appDetails.add(new AppDetails(context.getString(R.string.reddit_app_name), "com.reddit.frontpage", R.drawable.reddit_icon));
        appDetails.add(new AppDetails(context.getString(R.string.messenger_app_name), "com.facebook.orca", R.drawable.messenger_icon));
        appDetails.add(new AppDetails(context.getString(R.string.whatsapp_app_name), "com.whatsapp", R.drawable.whatsapp_icon));
        appDetails.add(new AppDetails(context.getString(R.string.youtube_app_name), "com.google.android.youtube", R.drawable.youtube_icon));
        appDetails.add(new AppDetails(context.getString(R.string.shazam_app_name), "com.shazam.android", R.drawable.shazam_icon));
        appDetails.add(new AppDetails(context.getString(R.string.hangouts_app_name), "com.google.android.talk", R.drawable.hangouts_icon));
        appDetails.add(new AppDetails(context.getString(R.string.spotify_app_name), "com.spotify.music", R.drawable.spotify_icon));
        appDetails.add(new AppDetails(context.getString(R.string.firefox_app_name), "org.mozilla.firefox", R.drawable.firefox_icon));
        appDetails.add(new AppDetails(context.getString(R.string.netflix_app_name), "com.netflix.mediaclient", R.drawable.netflix_icon));
        appDetails.add(new AppDetails(context.getString(R.string.pokemongo_app_name), "com.nianticlabs.pokemongo", R.drawable.pokemongo_icon));
    }

    /*Returns the app details arraylist*/
    public ArrayList<AppDetails> getAppDetails() {
        return new ArrayList<>(appDetails);
    }
}
