package org.privacyhelper.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * AppDetails
 * A holder class for all of the app details collected from an online source
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class AppDetails implements Parcelable {
    //Define all app detail variables
    private String appName;
    private String packageName;
    private int bitmapId;
    private String[] permissions = {};  //start with an empty array

    //Basic constructor that only sets the initial title, package name and bitmap id
    AppDetails(String initTitle, String initPackageName, int initBitmapId) {
        this.appName = initTitle;
        this.packageName = initPackageName;
        this.bitmapId = initBitmapId;
    }

    //Constructor to support parcelable interface
    private AppDetails(Parcel in) {
        appName = in.readString();
        packageName = in.readString();
        bitmapId = in.readInt();
        permissions = in.createStringArray();
    }

    //Creator constant to support parcelable interface
    public static final Creator<AppDetails> CREATOR = new Creator<AppDetails>() {
        @Override
        public AppDetails createFromParcel(Parcel in) {
            return new AppDetails(in);
        }

        @Override
        public AppDetails[] newArray(int size) {
            return new AppDetails[size];
        }
    };

    public void setPermissions(String[] permissions) {
        //No point setting to new array if not empty
        if (permissions.length > 0){
            this.permissions = permissions;
        }
    }

    @Override
    public int describeContents() {
        return 0;   //not used here
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(appName);
        dest.writeString(packageName);
        dest.writeInt(bitmapId);
        dest.writeStringArray(permissions);
    }

    /*Gets the app name*/
    public String getAppName() {
        return appName;
    }

    /*Gets the app package name*/
    public String getPackageName() {
        return packageName;
    }

    /*Gets the bitmap id*/
    public int getBitmapId() {
        return bitmapId;
    }

    /*Gets the app permissions array*/
    public String[] getPermissions() {
        //Don't directly return the array due to referencing issues
        String[] tmp = new String[permissions.length];
        System.arraycopy(permissions, 0, tmp, 0, permissions.length);
        return tmp;
    }
}
