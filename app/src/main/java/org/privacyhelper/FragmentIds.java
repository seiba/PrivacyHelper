package org.privacyhelper;

/**
 * FragmentIds
 * Class to hold the ids for each fragment
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class FragmentIds {
    public static final String privacy_basics_fragment_id = "privacy_basics";
    public static final String privacy_overview_fragment_id = "privacy_overview";
    public static final String avoid_apps_fragment_id = "avoid_apps";
}
