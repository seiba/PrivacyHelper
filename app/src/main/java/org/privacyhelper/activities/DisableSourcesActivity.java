package org.privacyhelper.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.privacyhelper.R;

/**
 * DisableSourceActivity
 * Class for explaining how to disable unknown sources
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class DisableSourcesActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disable_sources);

        Button securityButton = (Button) findViewById(R.id.securityMenuButton);

        //Add click action for the button
        securityButton.setOnClickListener(this);

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onClick(View v){
        int id = v.getId(); //get the view id

        if (id == R.id.securityMenuButton) {        //if security menu button
            launchSettings();   //open the required settings to manage the permissions
        }

    }

    /*Display the required menu depending on platform*/
    private void launchSettings() {
        Intent intent;  //the intent to use

        //Check what version of android is being used
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {   //Android Oreo or later
            intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
        } else {    //Android Nougat or earlier
            intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
        }

        //Activity exists, go to it
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {    //Doesn't exist, unable to open message
            Snackbar.make(findViewById(R.id.disableSourcesConstraintLayout), R.string.error_sec_settings_text, Snackbar.LENGTH_LONG).show();
        }
    }
}
