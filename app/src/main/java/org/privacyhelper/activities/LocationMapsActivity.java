package org.privacyhelper.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.TilesOverlay;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;
import org.privacyhelper.R;

import static org.privacyhelper.activities.SettingsActivity.KEY_THEME_PREF_LIST;

/**
 * LocationMapsActivity
 * A class for displaying Maps activity using the osmdroid API (OpenStreetMaps)
 * @author Joshua
 * @version 1
 * Modified: 24/06/18
 * Copyright (c) 2018
 */
public class LocationMapsActivity extends AppCompatActivity implements LocationListener {

    //Min update interval and distance for location service
    private static final long MIN_UPDATE_INTERVAL = 5000; //5 seconds
    private static final long MIN_UPDATE_DISTANCE = 1000; //1 kilometer
    //Request code for location permissions
    private static final int LOCATION_REQUEST_CODE = 1337;
    //Request code for location menu settings
    private static final int LOCATION_SETTINGS_CODE = 1012;
    //Focus factor for focusing
    private static final double zoomFactor = 15.0;

    //Declare class variables
    private static MapView mMapView = null;    //holds the openstreetmap mapview
    private static boolean mCanAccessLocation = false; //holds location access state
    private static GeoPoint mCurrentLocation = new GeoPoint(-37.8770, 145.0443);   //holds current location (GeoPoint object)
    private TilesOverlay invertOverlay; //holds the tileoverlay that inverts map colours

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //initialize the map config
        Context ctx = getApplicationContext();
        org.osmdroid.config.Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        //inflate and create the map
        setContentView(R.layout.activity_location_maps);

        mMapView = (MapView) findViewById(R.id.osm_map);

        //Initialize map interactions and points
        initMap();

        mCanAccessLocation = (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);

        // If we don't have permissions then request them
        if (!mCanAccessLocation) {
            requestPermissions();
        }

        // Initialize starting map marker location
        initLocation();

        //Add support for back navigation action item
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initLocation() {
        /*
        Initializes the starting marker location on map
         */
        if (mCanAccessLocation) {   //has sufficient location permissions
            boolean locationSame = checkLocationSame();
            if (!locationSame || getCurrentLoc()) {   //location not same as defaults
                Toast.makeText(this, getString(R.string.tap_marker_text), Toast.LENGTH_SHORT).show();
            }
        }
        createMarker(); //create the marker regardless of scenario
    }

    private boolean getCurrentLoc() {
        /*
        Attempt to get location of user
         */
        boolean result = false; //needed to ensure can request location services
        LocationManager locMan = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locMan != null) {   //location manager exists
            /*
            Check the location permission again for sanity check
             */
            mCanAccessLocation = (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
            if (mCanAccessLocation) {
                locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_UPDATE_INTERVAL, MIN_UPDATE_DISTANCE, this);    //force update for location
                result = true;
            }
        }
        return result;
    }

    /*Initializes the default map for this acticity*/
    private void initMap() {
        //Set map behavior/appearance
        mMapView.setTileSource(TileSourceFactory.MAPNIK);
        mMapView.setMultiTouchControls(true);
        mMapView.setTilesScaledToDpi(true);

        //Add rotation gesture support
        RotationGestureOverlay rotateOverlay = new RotationGestureOverlay(mMapView);
        rotateOverlay.setEnabled(true);
        mMapView.setMultiTouchControls(true);
        mMapView.getOverlays().add(rotateOverlay);

        //Apply the app theme to maps
        applyAppTheme();
    }

    /*Request for location access permissions*/
    private void requestPermissions() {
        // check if we need permission for fine location
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            new AlertDialog.Builder(LocationMapsActivity.this)
                    .setTitle(R.string.request_dialog_title)
                    .setMessage(R.string.location_request_message)
                    .setPositiveButton(R.string.allow_dialog_action, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(LocationMapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
                        }
                    })
                    .setNegativeButton(R.string.cancel_dialog_action, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            finish();
                        }
                    })
                    .show();
        } else {
            //we don't need to show the user info we can just request permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }
    }

    /*Checks if high accuracy location services is enabled. Returns a boolean*/
    private boolean highAccuracyEnabled() {
        /*
         * The following code checks if location services has been enabled as per suggested from stackoverflow
         * https://stackoverflow.com/questions/10311834/how-to-check-if-location-services-are-enabled
         *
         * Modified by: Joshua Lay
         *
         * All rights reserved
         * */
        boolean response = false;   //assume disabled by default
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Initialize booleans for gps and network location functionality

        if (lm != null) {   //if location manager exists
            //Get the location service states
            response = lm.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                    lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } else {    //no location manager found
            Log.e("LocationManager", "No location manager found");
        }
        return response;
    }

    /*Just shows the location settings activity for the user*/
    private void showLocationSettings() {
        // notify user
        new AlertDialog.Builder(LocationMapsActivity.this)
            .setMessage(R.string.location_settings_message)
            .setPositiveButton(R.string.dialog_open_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    if (myIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(myIntent, LOCATION_SETTINGS_CODE);
                    }
                }
            })
            .setNegativeButton(R.string.cancel_dialog_action, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    //Just cancel the alertdialog
                    paramDialogInterface.cancel();

                }
            })
            .setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    //Display snackbar when dialog is dismissed (tapping elsewhere) or canceled
                    Snackbar.make(findViewById(R.id.locationConstraintLayout), R.string.permissions_error_message, Snackbar.LENGTH_LONG)
                            .show();
                }
            })
            .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*
        Override activity result handler to check if location settings was updated
         */

        // Check which request we're responding to
        if (requestCode == LOCATION_SETTINGS_CODE) {    //Location settings activity
            // Check if high accuracy was enabled
            if (highAccuracyEnabled()) {   //High accuracy enabled
                useCurrentLoc();    //Use current location
            } else {    //Not enabled
                //Display snackbar when dialog is dismissed (tapping elsewhere) or canceled
                Snackbar.make(findViewById(R.id.locationConstraintLayout), R.string.permissions_error_message, Snackbar.LENGTH_LONG)
                        .show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        //Check with request code has been given to us
        switch (requestCode) {
            case LOCATION_REQUEST_CODE:
                // This is a location permission request so lets handle it
                if (grantResults.length > 0) {
                    //Can access coarse is equal to
                    mCanAccessLocation = (grantResults[0] == PackageManager.PERMISSION_GRANTED);
                    if (mCanAccessLocation) {   //permissions given to location services
                        if (!highAccuracyEnabled()) //high accuracy gps disabled
                            showLocationSettings(); //show the required settings menu
                    }
                }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_activity_location, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_location:  //location pin item selected
                useCurrentLoc();
                return true;
            case R.id.action_info:  //info icon item selected
                startActivity(new Intent(this, LocationHelpActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*Uses the current user location if possible*/
    private void useCurrentLoc() {
        //Check if location permissions enabled
        if (mCanAccessLocation) {
            //Check again if the user has enabled location
            if (highAccuracyEnabled()) {  //high accuracy enabled
                /*Attempt to set focus to current location*/
                if (checkLocationSame()) { //same location
                    Toast.makeText(this, getString(R.string.tap_wait_text), Toast.LENGTH_SHORT).show();
                    getCurrentLoc();
                } else {    //not same
                    Toast.makeText(this, getString(R.string.tap_marker_text), Toast.LENGTH_SHORT).show();
                    createMarker();  //focus on current location
                }
            } else {    //not enabled
                //show location settings for user
                showLocationSettings();
            }
        }
    }

    /*Focuses on the user's current known location*/
    private void createMarker() {
        /*
        Create a new marker with text. Then center the map to the point
         */
        Marker currentPoint = new Marker(mMapView);
        currentPoint.setPosition(mCurrentLocation);
        currentPoint.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        currentPoint.setTitle(getString(R.string.maps_point_select_text));
        mMapView.getOverlays().add(currentPoint);

        //Update focus point
        setFocus();
    }

    private void setFocus() {
        /*
        Sets the focus to the current location pointed
        */
        mMapView.getController().setZoom(zoomFactor);
        mMapView.getController().setCenter(mCurrentLocation);
    }

    private boolean checkLocationSame() {
        /*
        Checks to see if current location is same as default value
        If same, true. Else, false
         */
        final double lat = -37.8770, lon = 145.0443;  //location of Monash caulfield
        return (mCurrentLocation.getLatitude() == lat && mCurrentLocation.getLongitude() == lon);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mMapView != null) {
            mMapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mMapView != null) {
            mMapView.onPause();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        /*
        Handles the location changed event on location fix
         */
        Toast.makeText(this, getString(R.string.tap_marker_text), Toast.LENGTH_SHORT).show();
        mCurrentLocation = new GeoPoint(location);
        createMarker();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        //not used
    }

    @Override
    public void onProviderEnabled(String provider) {
        //not used
    }

    @Override
    public void onProviderDisabled(String provider) {
        //not used
    }

    /*Applies the app theme to the map element*/
    private void applyAppTheme() {
        //Grab the selection from sharedpreferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String display_theme = sharedPreferences.getString(KEY_THEME_PREF_LIST, "");

        //Check which theme was set
        switch (display_theme) {
            case "Dark":   //Dark theme
                if (invertOverlay == null) {
                    //Create a tile overlay that uses the inverted theme
                    TilesOverlay overlay = new TilesOverlay(mMapView.getTileProvider(), this);
                    overlay.setColorFilter(TilesOverlay.INVERT_COLORS);
                    mMapView.getOverlays().add(overlay);
                    //Store a copy of the overlay for later deletion (if required)
                    invertOverlay = overlay;
                    break;
                }
            default:    //Light theme
                if (invertOverlay != null)  //if invert tile overlay exists
                    mMapView.getOverlays().remove(invertOverlay);   //remove it
        }
    }
}
