package org.privacyhelper.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import org.privacyhelper.R;
import org.privacyhelper.adapters.ClickListener;
import org.privacyhelper.adapters.RAdapter;

/**
 * TechniquesActivity
 * Displays a list of mitigation techniques for the user to try
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class TechniquesActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        //get the recycler view
        mRecyclerView = (RecyclerView) findViewById(R.id.rView);
        setRecyclerViewOptions();

        //Create an array containing the strings of the activities that can be displayed
        String[] mitigationTitles = {getString(R.string.disable_permissions_text), getString(R.string.disable_unknown_sources_text), getString(R.string.disable_unused_app_title)};

        //specify the adapter for the current view
        RAdapter mAdapter = new RAdapter(mitigationTitles, R.id.itemConstraintLayout);
        mRecyclerView.setAdapter(mAdapter);

        //Add action to recyclerview item click
        mAdapter.setClickListener(new ClickListener() {
            @Override
            public void itemClicked(View view, int position) {
                launchMenuItem(position);   //call the method handling intents
            }
        });

        //Disable swipe to refresh
        findViewById(R.id.swipe_container).setEnabled(false);

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            /*Set the home action icon to home icon*/
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_home);
        }

    }

    /*Initializes recyclerview properties*/
    private void setRecyclerViewOptions() {
        //using recommended setting to improve perf
        mRecyclerView.setHasFixedSize(true);

        //use linear layout
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //Add line decoration to make it more clear that the list has touch interactions
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(),  mLayoutManager.getOrientation()));
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    /*Method to handle the intent to provide view switching between info.*/
    private void launchMenuItem(int position) {
        //Define destination activity
        Class dest;

        //Check if destination activity required
        switch (position) {
            case 0: //Disable permissions activity selected
                dest = DisablePermissionsActivity.class;
                break;
            case 1: //Disable unknown sources activity selected
                dest = DisableSourcesActivity.class;
                break;
            case 2: //Disable unused apps activity selected
                dest = DisableUnusedActivity.class;
                break;
            default:    //something wrong here
                Log.e("PrivacyHelper", "Problem with recycler view click handler");
                dest = null;
        }

        //If destination activity required
        if (dest != null) {
            startActivity(new Intent(this, dest));
        }
    }

}
