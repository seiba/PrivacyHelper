package org.privacyhelper.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.privacyhelper.R;

/**
 * DisablePermissionsActivity
 * Class for explaining how to disable permissions
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class DisablePermissionsActivity extends AppCompatActivity implements View.OnClickListener{

    // The textview that will hold the android version details
    TextView versionString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disable_permissions);

        Button listApplicationsButton = (Button) findViewById(R.id.listApplicationsButton);
        versionString = (TextView) findViewById(R.id.versionTextView);

        //Check the android version if suitable
        checkAndroidVersion();

        //Add tap action for the buttons
        listApplicationsButton.setOnClickListener(this);

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    /*Updates the version check text and display prompt if android version before Marshmallow*/
    private void checkAndroidVersion() {
        //Declare variable to hold the colour
        int colour;
        //Declare variable to hold the version text to overwrite with
        String versionText;

        //Check if android version M or newer
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Android Marshmallow and newer = fine
            versionText = getString(R.string.android_version_new_text);
            colour = ResourcesCompat.getColor(getResources(), R.color.colorVersionNew, null);
        } else {
            //Android Lollipop and older = not fine
            versionText = getString(R.string.android_version_old_text);
            colour = ResourcesCompat.getColor(getResources(), R.color.colorVersionOld, null);

            /*Use a toast to alert the user*/
            Toast.makeText(this, R.string.old_android_ver_text, Toast.LENGTH_SHORT).show();
        }
        //Adjust version info string based on android version
        versionString.setText(versionText);
        versionString.setTextColor(colour);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        //Get the view id
        int id = v.getId();
        //If list applications button was clicked
        if (id == R.id.listApplicationsButton) {
            //Attempt to open list of applications menu
            Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
            if (intent.resolveActivity(getPackageManager()) != null) {
                //List of applications menu present
                startActivity(intent);
            }
            else {
                //List of applications menu not found
                Snackbar.make(findViewById(R.id.permissionsMitigateConstraintLayout), R.string.error_list_apps_text, Snackbar.LENGTH_LONG)
                    .show();
            }
        }
    }
}
