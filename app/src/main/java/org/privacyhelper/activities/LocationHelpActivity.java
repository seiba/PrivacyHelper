package org.privacyhelper.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.privacyhelper.R;

/**
 * LocationHelperActivity
 * Class to provide more information on the maps activity
 * @author Joshua Lay
 * @version 1
 * Modified: 24/05/18
 * Copyright (c) 2018
 */
public class LocationHelpActivity extends AppCompatActivity {

    //Link to the Google Location timeline webpage
    private static final String googleURL = "https://www.google.com.au/maps/timeline";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_help);

        //Initialize button
        Button browserOpenButton = (Button) findViewById(R.id.browserButton);

        //Set button onclick action
        browserOpenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open the link in Google Maps (if installed)
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(googleURL));
                startActivity(intent);
            }
        });

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
