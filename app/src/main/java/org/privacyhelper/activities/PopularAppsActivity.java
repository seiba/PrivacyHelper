package org.privacyhelper.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import org.privacyhelper.BitmapDownscaler;
import org.privacyhelper.R;
import org.privacyhelper.adapters.ClickListener;
import org.privacyhelper.adapters.RIconAdapter;
import org.privacyhelper.models.AppDetails;
import org.privacyhelper.models.AppList;

import java.util.ArrayList;

/**
 * PopularAppsActivity
 * A class for listing apps in the RAdapter
 * @author Joshua
 * @version 1
 * Modified: 18/04/18
 * Copyright (c) 2018
 */
public class PopularAppsActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    //declare recyclerview variable to hold list of app names
    private RecyclerView mRecyclerView;
    private RIconAdapter rIconAdapter;

    //initialize an arraylist to hold all of the app details
    private ArrayList<AppDetails> appList = new ArrayList<>();
    //array to hold app icons
    private Bitmap[] bitmaps = {};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        //get the recycler view
        mRecyclerView = (RecyclerView) findViewById(R.id.rView);
        setRecyclerViewOptions();


        //intialize the app list with some app details objects
        initAppList();

        //intialize the adapter
        initAdapter();

        //Disable swipe to refresh
        findViewById(R.id.swipe_container).setEnabled(false);

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    /*Initializes recyclerview properties*/
    private void setRecyclerViewOptions() {
        //using recommended setting to improve perf
        mRecyclerView.setHasFixedSize(true);

        //use linear layout
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //Add line decoration to make it more clear that the list has touch interactions
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(),  mLayoutManager.getOrientation()));
    }

    /*Initializes the adapter with the app names*/
    private void initAdapter() {
        //copy all of the app names only to a normal java array object
        String[] appNames = new String[appList.size()];
        for (int i = 0; i < appNames.length; i++) {
            appNames[i] = appList.get(i).getAppName();
        }

        //specify the adapter for the current view
        rIconAdapter = new RIconAdapter(appNames, bitmaps, R.id.rView);
        mRecyclerView.setAdapter(rIconAdapter);

        //Add action to recyclerview item click
        rIconAdapter.setClickListener(new ClickListener() {
            @Override
            public void itemClicked(View view, int position) {
                launchMenuItem(position);   //call the method handling intents
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    /*Method to handle the intent to provide view switching between info.*/
    private void launchMenuItem(int position) {
        Intent intent;

        //Now we need to grab the filtered index, rather than from the original list
        int[] indexes = rIconAdapter.getFilteredIndexes();
        if (indexes.length > 0) {   //not empty
            position = indexes[position];   //position is now given from the filtered index
        }

        intent = new Intent(this, PermissionsListActivity.class);
        intent.putParcelableArrayListExtra(getString(R.string.app_arraylist_intent), appList); //needed so that permissionsactivity has own copy of applist
        intent.putExtra(getString(R.string.app_position_intent), position);   //needed so that permissionsactivity knows which app to look at
        startActivity(intent);
    }

    /*Initializes appList with AppDetails objects containing basic info about some apps*/
    private void initAppList() {
        /*
        Array of image resource ids needed to convert each to a bitmap
        Intellectual property belongs to their respectful owners
        All rights reserved
        */

        /*Create app list object then retrieve app details*/
        appList = new AppList(this).getAppDetails();

        //IMPORTANT: convert all image icons to bitmaps for much faster loading
        bitmaps = new Bitmap[appList.size()];
        //Get the required dimension for the app icon
        int dimen = getResources().getInteger(R.integer.app_icon_dimen);

        //For every app, convert the bitmap to a more suitable resolution
        for (int i = 0; i < appList.size(); i++) {
            bitmaps[i] = BitmapDownscaler.decodeSampledBitmapFromResource(getResources(), appList.get(i).getBitmapId(), dimen, dimen);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);

        //Initialize search menu item behavior
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView mSearchView = (SearchView) searchItem.getActionView();
        if (manager != null)
            mSearchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        else
            Log.e("Search", "No manager to provide search info");
        mSearchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        rIconAdapter.getFilter().filter(newText);
        return true;
    }
}
