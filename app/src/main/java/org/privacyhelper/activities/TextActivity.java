package org.privacyhelper.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.privacyhelper.FragmentIds;
import org.privacyhelper.R;
import org.privacyhelper.fragments.AvoidAppsFragment;
import org.privacyhelper.fragments.PrivacyIntroFragment;
import org.privacyhelper.fragments.PrivacyOverviewFragment;

/**
 * TextActivity
 * A class to manage fragments that only contain textviews
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class TextActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        String val = intent.getStringExtra(getString(R.string.text_activity_intent_key));

        FragmentManager fm = getSupportFragmentManager();

        switch (val) {
            case FragmentIds.privacy_basics_fragment_id: //data privacy basics fragment
                fm.beginTransaction().replace(android.R.id.content, new PrivacyIntroFragment())
                        .commit();
                setTitle(getString(R.string.privacy_basics_text));
                break;
            case FragmentIds.privacy_overview_fragment_id: //app privacy overview fragment
                fm.beginTransaction().replace(android.R.id.content, new PrivacyOverviewFragment())
                        .commit();
                setTitle(getString(R.string.app_privacy_overview_text));
                break;
            case FragmentIds.avoid_apps_fragment_id: //apps to avoid fragment
                fm.beginTransaction().replace(android.R.id.content, new AvoidAppsFragment())
                        .commit();
                setTitle(getString(R.string.apps_avoid_activity_text));
                break;
            default: //something else
                Log.e("TextFragment", "Incorrect fragment identifier supplied");
        }

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (val.equals(FragmentIds.privacy_basics_fragment_id) || val.equals(FragmentIds.privacy_overview_fragment_id)) { //
                /*Set the home action icon to home icon*/
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_home);
            }
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
