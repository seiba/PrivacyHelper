package org.privacyhelper.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.privacyhelper.R;
import org.privacyhelper.adapters.RAdapter;
import org.privacyhelper.models.AppDetails;
import org.privacyhelper.models.PermissionList;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * PermissionsListActivity
 * Lists the dangerous permissions of the selected application
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class PermissionsListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, SwipeRefreshLayout.OnRefreshListener{

    //Define all class variables
    private int position;
    private RecyclerView mRecyclerView; //The recyclerview
    private RAdapter rAdapter;  //The recyclerview adapter
    private SwipeRefreshLayout mSwipeRefreshLayout; //The swipe to refresh layout

    // The arraylist to store all of the appdetail objects
    private static ArrayList<AppDetails> appList = new ArrayList<>();
    // Boolean to keep track if permissions have been updated
    private static boolean permissionUpdated = false;
    // Define timeout to use before canceling Async Download
    private static final int async_timeout = 15000;
    // Define variable to keep track of query string
    private String filter_constraint;

    // Define the JSON download link needed to download the JSON permissions data
    private static final String JSON_DOWNLOAD_LOCATION = "https://gitlab.com/seiba/PrivacyHelper-PermissionsJSON/raw/master/permissions.json";

    // Define the filename for the JSON string file in local storage
    private static final String filename = "string_permissions_list";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //get the recycler view
        mRecyclerView = (RecyclerView) findViewById(R.id.rView);
        setRecyclerViewOptions();

        //get the swiperefresh layout
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        //Get default error value from resources
        int errVal = getResources().getInteger(R.integer.default_err_val);

        //grab the parcelable extras from previous activity
        Intent intent = getIntent();
        position = intent.getIntExtra(getString(R.string.app_position_intent), errVal);

        //check if the permissions data has already been fetched
        if (position == errVal) {  //error in communication between activities
            Log.e("PrivacyHelper", "Was not able to send app list clicked position");
        }
        else {  //position number is valid
            if (appList.size() <= 0) {  //app list not initialized
                appList = intent.getParcelableArrayListExtra(getString(R.string.app_arraylist_intent));
            }
            //assume appList has something, get app permissions ready
            initPermissions();
        }
    }

    /*Initializes recyclerview properties*/
    private void setRecyclerViewOptions() {
        //using recommended setting to improve perf
        mRecyclerView.setHasFixedSize(true);

        //use linear layout
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //Add line decoration to make it more clear that the list has touch interactions
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(),  mLayoutManager.getOrientation()));
    }

    /*Initializes the permission from local or downloads it from online*/
    private void initPermissions() {
        // Check if app list was changed
        if (appList.size() > 0) {   //app list changed
            //Check if permissions have already been updated
            if (permissionUpdated) {  //permissions updated, proceed to show the permissions
                initAdapter();
            }
            else if (isInternetConnected() && checkJSONOld()) {   //permissions not updated, internet connection available and JSON is old enough
                scheduleAsyncDownload();
            }
            else {  //permissions not updated, no internet connection
                useLocalData();  //use local permission copy
            }
        } else {    //not changed
            //Something happened when passing the applist through context
            Snackbar.make(findViewById(R.id.rViewCLayout), R.string.processing_applist_sb_text, Toast.LENGTH_LONG).show();
        }
    }

    /*Checks to see if a week has passed since last fetch from online source*/
    private boolean checkJSONOld() {
        //Initialize method variables
        boolean result = false; //default to JSON as new
        long timestamp = -1;    //default to timestamp as error val

        //Store the quiz result to sharedpreferences in private context mode
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.timestamp_shared_pref), Context.MODE_PRIVATE);

        //Check if sharedpreference exists
        if (sharedPref != null) {
            timestamp = sharedPref.getLong(getString(R.string.saved_timestamp_key), -1);
        }

        //Check if timestamp valid
        if (timestamp == -1) {  //timestamp NOT present
            result = true;
        } else {    //timestamp present
            //Get the update frequency preference string
            SharedPreferences defaultSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            String listPref = defaultSharedPref.getString(SettingsActivity.KEY_PREF_FREQUENCY_LIST, "");   //default to weekly

            /*Check to see if a week has passed since last update*/
            Calendar previous = Calendar.getInstance();
            Calendar current = Calendar.getInstance();
            previous.setTimeInMillis(timestamp);

            //Change threshold date based on preferences
            switch (listPref) {
                case "Always":   //always
                    return true;
                case "Never":   //never
                    return false;
                case "Daily":   //daily
                    previous.add(Calendar.DAY_OF_YEAR, 1);
                    break;
                case "Monthly":   //monthly
                    previous.add(Calendar.MONTH, 1);
                    break;
                default:    //weekly
                    previous.add(Calendar.WEEK_OF_YEAR, 1);
            }

            //Compare between the two dates
            if (current.compareTo(previous) > 0) {  //week has passed
                result = true;
            }
        }

        return result;
    }

    /*Updates the timestamp of when the JSON was last checked for updates*/
    private void updateSavedTimestamp() {
        Calendar cal = Calendar.getInstance();
        long timestamp = cal.getTimeInMillis();

        //Store the quiz result to sharedpreferences in private context mode
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.timestamp_shared_pref), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(getString(R.string.saved_timestamp_key), timestamp).apply();
    }

    /*Checks to see if there is an active internet connection or not*/
    private boolean isInternetConnected() {
        boolean response = false;
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null) {
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            response = activeNetwork != null &&
                    activeNetwork.isConnected();
        }

        return response;
    }


    /*Schedules downloadPermissionsJSON with a timeout*/
    private void scheduleAsyncDownload() {
        /*
        Schedules async download task and cancels it if it can't complete within a timeout value
        Original idea: https://stackoverflow.com/questions/7882739/android-setting-a-timeout-for-an-asynctask
        Modified by: Joshua Lay
         */

        //New instance of downloadPermissionsJSON
        final downloadPermissionsJSON downloader = new downloadPermissionsJSON(this);
        //Launch the instance
        downloader.execute(JSON_DOWNLOAD_LOCATION);
        //Define a timeout handler
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /*
                Two cases to consider:
                1. Download taking too long (slow internet connection?)
                2. Something happened during the processing (unlikely, but could happen)

                For either case, try using a stored JSON. The info will be out-of-date, but better
                than nothing.
                 */

                //If background task still running
                if (downloader.getStatus() == AsyncTask.Status.RUNNING) {
                    //Cancel it
                    downloader.cancel(true);
                    mSwipeRefreshLayout.setRefreshing(false);   //disable refreshing animation
                    //Attempt to use stored JSON values
                    useLocalData();
                }
            }
        }, async_timeout);  //Give about 10 seconds to complete everything
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filter_constraint = newText;   //store a copy of the filter constraint
        rAdapter.getFilter().filter(newText);
        return true;
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);    //enable refreshing animation
        // Fetching data from server
        if (isInternetConnected()) {
            scheduleAsyncDownload();
        } else {
            mSwipeRefreshLayout.setRefreshing(false);   //disable refreshing animation
            /*Display error snackbar dialog to user*/
            Snackbar.make(findViewById(R.id.rViewCLayout), R.string.error_fetch_text, Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    /*Class to download the JSON file from the internet
    *
    * Downloading of the data should only be done once while in the app.
    * This needs to be done every time a new instance of the app is created.
    * */
    private static class downloadPermissionsJSON extends AsyncTask<String, Void, String> {

        //Store the weak reference of this activity class
        private WeakReference<PermissionsListActivity> activityReference;

        //Constructor for this async task downloader
        downloadPermissionsJSON(PermissionsListActivity context) {
            //initialize activity reference with outer activity class context
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
            //Before do in background called (UI Thread), just inform user
            Toast.makeText(activityReference.get(), R.string.fetching_perms_message_text, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {   //attempt to download the JSON string
                URL downloadURL = new URL(strings[0]);
                HttpURLConnection connection = (HttpURLConnection) downloadURL.openConnection();
                InputStream input = connection.getInputStream();

                String result;
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder sb = new StringBuilder();

                while ((result = reader.readLine()) != null) {
                    sb.append(result);
                }

                //return a string that represents the downloaded json
                return sb.toString();
            }
            catch (Exception e) {   //something happened to the download
                e.printStackTrace();
            }

            //just return a null on error
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            PermissionsListActivity activity = activityReference.get();
            if (result != null) {   //can download
                try {
                    //Get the result
                    JSONObject object = new JSONObject(result);
                    //Get the online JSON revision
                    int revision =  object.optInt("revision", -1);

                    //Attempt to get the shared preference file
                    SharedPreferences sharedPref = activity.getSharedPreferences(activity.getString(R.string.revision_shared_pref), Context.MODE_PRIVATE);
                    //Get the previous score from the shared preference
                    int previous_revision = sharedPref.getInt(activity.getString(R.string.saved_revision_key), -1);

                    //Check revision doesn't match or not found
                    if (previous_revision == -1 || (revision != -1 && previous_revision != revision)) { //revision doesn't match or not found
                        //Store the quiz result to sharedpreferences in private context mode
                        SharedPreferences sharedPreferences = activity.getSharedPreferences(activity.getString(R.string.revision_shared_pref), Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt(activity.getString(R.string.saved_revision_key), revision).apply();

                        //Save the JSON locally
                        activity.saveJSONfile(result);
                        //Do all processing and cleanups
                        activity.processJSONdata(result);
                    } else {    //revision matched and found
                        //Update the saved timestamp (no updates from online source)
                        activity.updateSavedTimestamp();
                        //Do all processing and cleanups (for local JSON)
                        activity.useLocalData();
                    }
                }
                catch (Exception e) {   //problem occurred when processing json object
                    e.printStackTrace();
                }
            }
            else {  //JSON file wasn't possible to download ()
                activity.useLocalData();
            }
        }
    }

    /*Attempts to use the stored JSON information (if applicable)*/
    private void useLocalData() {
        try {
            String stored_result = getJSONFromFile();
            //do all processing and cleanups
            processJSONdata(stored_result);    //try to open local copy of JSON
            Toast.makeText(this, R.string.local_json_message_text, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            //unable to open
            e.printStackTrace();
            /*Display error snackbar dialog to user*/
            Snackbar.make(findViewById(R.id.rViewCLayout), R.string.error_fetch_text, Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    /*Processes the local JSON file*/
    private void processJSONdata(String result) throws Exception {
        //if string retrieved...
        if (result != null) {   //permissions in internal storage, use that
            // Convert to JSON object and extract contents
            processPermissions(new JSONObject(result));
            //Only after the download should we intialize the adapter
            initAdapter();
        } else {
            Log.e("JSON process", "Attempted to open non-existent JSON string object");
        }
    }

    /*Updates the permissions list for all apps given the JSON object*/
    private void processPermissions(JSONObject appObject) throws Exception {
        // For every app in the JSONObject
        for (int i = 0; i < appList.size(); i++) {
            //Get json permissions array for each app
            JSONArray tmpPermissions = appObject.getJSONArray(appList.get(i).getPackageName());
            String[] permissionsArray = new String[tmpPermissions.length()];
            // For every permission for that app, copy to an array
            for (int j = 0; j < tmpPermissions.length(); j++) {
                permissionsArray[j] = tmpPermissions.getString(j);
            }

            // Make sure to decode all permissions to a human readable form
            permissionsArray = decodePermissionsCodes(permissionsArray);

            // Update the app's permissions
            appList.get(i).setPermissions(permissionsArray);
        }

        //Update variable to say permissions have been updated
        permissionUpdated = true;
    }

    /*Opens the json file from internal storage*/
    private String getJSONFromFile() {
        // To buffer for stream result
        String buffer;
        // The variable to hold the input stream
        FileInputStream inputStream;
        // Check if existing file is present
        try {
            //Open the file as a stream
            inputStream = openFileInput(filename);
            //Create new buffer reader
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            //Create new string builder
            StringBuilder sb = new StringBuilder();

            //While something can be read...
            while ((buffer = reader.readLine()) != null) {
                sb.append(buffer);
            }

            //Close the inputstream
            inputStream.close();

            //Return actual string that was processed
            return sb.toString();
        } catch (Exception e) {
            //Unable to open file (e.g. not located)
            e.printStackTrace();
            //Just return null
            return null;
        }
    }

    /*Saves the permissions JSON to local storage*/
    private void saveJSONfile(String s) throws Exception {
        // Update the timestamp for checking
        updateSavedTimestamp();

        // To hold the output stream
        FileOutputStream outputStream;

        //Store the JSON into string_permissions_list file
        outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
        outputStream.write(s.getBytes());
        outputStream.close();
    }

    //Decodes only the dangerous permissions info taken from the JSON to a human readable description
    private String[] decodePermissionsCodes(String[] oldPermissions) {
        //Grab the permissions info from the permissionlist class object
        HashMap<String, String> permissionInfo = new PermissionList(this).getPermissionInfo();

        //Define a tmp arraylist to hold the decoded permission descriptions
        ArrayList<String> tmp = new ArrayList<>();

        //Attempt to fetch each permission description
        for (String s: oldPermissions) {
            //Check if key exists in permission hashmap
            if (permissionInfo.containsKey(s)) {
                //Add the description to the arraylist
                tmp.add(permissionInfo.get(s));
            }
        }

        //Create a new array to hold the descriptions (needed for app object)
        String[] newPermissions = new String[tmp.size()];

        //Copy arraylist items to array
        newPermissions = tmp.toArray(newPermissions);

        return newPermissions;
    }

    /*Extracted adapter intializer since we are dealing with async tasks*/
    private void initAdapter() {
        //Set up the adapter with the permissions for that particular app
        rAdapter = new RAdapter(appList.get(position).getPermissions(), R.id.itemConstraintLayout, false);
        mRecyclerView.setAdapter(rAdapter);

        //Filter with the constraint
        rAdapter.getFilter().filter(filter_constraint);

        mSwipeRefreshLayout.setRefreshing(false);   //remove refresh animation
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_info, menu);

        //Initialize search menu item behavior
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView mSearchView = (SearchView) searchItem.getActionView();
        if (manager != null)
            mSearchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        else
            Log.e("Search", "No manager to provide search info");
        mSearchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_info:  //info action item selected
                startActivity(new Intent(this, PermissionsHelpActivity.class));
                return true;    //do something here
            default:
                return super.onOptionsItemSelected(item);    //don't do anything here
        }


    }

}
