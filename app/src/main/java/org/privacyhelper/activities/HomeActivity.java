package org.privacyhelper.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.privacyhelper.R;
import org.privacyhelper.fragments.HomeFragmentTab;
import org.privacyhelper.fragments.LearnFragmentTab;

import static org.privacyhelper.activities.SettingsActivity.KEY_THEME_PREF_LIST;

/**
 * HomeActivity
 * A class for handling most of the tab view logic and other home screen elements
 * @author Joshua
 * @version 1
 * Modified: 18/04/18
 * Copyright (c) 2018
 */
public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    //Define number of page tabs available
    private static final int numSections = 2;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.homeTabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        //Add sharing action to the floating action button
        findViewById(R.id.fab_share).setOnClickListener(this);

        //Apply the app theme to the app
        applyAppTheme();
    }

    /*Applies the app theme throughout the whole app*/
    private void applyAppTheme() {
        //Grab the selection from sharedpreferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String display_theme = sharedPreferences.getString(KEY_THEME_PREF_LIST, "");

        //Check which theme was set
        switch (display_theme) {
            case "Dark":   //Dark theme
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                toolbar.setPopupTheme(R.style.AppTheme_PopupOverlay_Dark);
                break;
            default:    //Light theme
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                toolbar.setPopupTheme(R.style.AppTheme_PopupOverlay);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Get the view id
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:  //settings action selected
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.action_about: //about action selected
                //open the about app activity
                startActivity(new Intent(this, AboutActivity.class));
                return true;    //do something here
            default:
                return super.onOptionsItemSelected(item);    //don't do anything here
        }


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.fab_share) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND)
                .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_intent_message))
                .setType("text/plain");
            startActivity(Intent.createChooser(shareIntent, getString(R.string.send_to)));
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            //Get the section item
            switch (position) {
                case 0: //first tab selected
                    return new HomeFragmentTab();
                case 1: //second tab selected
                    return new LearnFragmentTab();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return numSections;
        }
    }

}
