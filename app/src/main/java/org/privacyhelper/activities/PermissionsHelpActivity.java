package org.privacyhelper.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.privacyhelper.R;

/**
 * PermissionsHelpActivity
 * Class for providing additional info on permissions
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class PermissionsHelpActivity extends AppCompatActivity implements View.OnClickListener {

    //Link to the Google permissions definitions
    private static final String googleHelpURL = "https://support.google.com/googleplay/answer/6014972?co=GENIE.Platform%3DAndroid&hl=en";
    //Link to Google Play Store
    private static final String playStoreURL = "https://play.google.com/store";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions_help);

        //Define and initialize buttons
        Button browserOpenButton = (Button) findViewById(R.id.openWebpageButton);
        Button playStoreButton = (Button) findViewById(R.id.playStoreButton);

        //click listeners to this activity
        browserOpenButton.setOnClickListener(this);
        playStoreButton.setOnClickListener(this);

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        //Get view id
        int id = v.getId();
        //Define view action intent
        Intent intent = new Intent(Intent.ACTION_VIEW);
        //Used to keep track if link has been assigned
        String url;

        //Check if item click is one of the buttons
        switch (id) {
            case R.id.openWebpageButton:    //open web page button
                url = googleHelpURL;
                break;
            case R.id.playStoreButton:  //open play store button
                url = playStoreURL;
                break;
            default:
                url = null;
        }

        if (url != null) {   //if intent is set
            intent.setData(Uri.parse(url));
            startActivity(intent);
        }
    }
}
