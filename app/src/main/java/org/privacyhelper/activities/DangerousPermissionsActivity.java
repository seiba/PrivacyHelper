package org.privacyhelper.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import org.privacyhelper.FragmentIds;
import org.privacyhelper.R;
import org.privacyhelper.adapters.ClickListener;
import org.privacyhelper.adapters.RAdapter;

/**
 * DangerousPermissionsActivity
 * Class for list out all of the things the user must be aware/or avoid
 * @author Joshua Lay
 * @version 1
 * Modified: 24/05/18
 * Copyright (c) 2018
 */
public class DangerousPermissionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            /*Set the home action icon to home icon*/
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_home);
        }


        //get the recycler view
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.rView);

        //using recommended setting to improve perf
        mRecyclerView.setHasFixedSize(true);

        //use linear layout
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //array of recyclerview string items
        String[] titleArray = {getString(R.string.location_tracking_text), getString(R.string.permissions_list_text), getString(R.string.apps_avoid_activity_text)};

        //specify the adapter for the current view
        RAdapter mAdapter = new RAdapter(titleArray, R.id.itemConstraintLayout);
        mRecyclerView.setAdapter(mAdapter);

        //Add line decoration to make it more clear that the list has touch interactions
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(),  mLayoutManager.getOrientation()));

        //Disable swipe to refresh
        findViewById(R.id.swipe_container).setEnabled(false);

        //Add action to recyclerview item click
        mAdapter.setClickListener(new ClickListener() {
            @Override
            public void itemClicked(View view, int position) {
                launchMenuItem(position);   //call the method handling intents
            }
        });
    }

    /*Allows the user to use action bar to go back*/
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    /*Method to handle the intent to provide view switching between info*/
    private void launchMenuItem(int position) {
        //Define class destination activity
        Class dest;
        String fragment_id = null;

        //Check if need to switch activities
        switch (position) {
            case 0: //go to the locations activity
                dest = LocationMapsActivity.class;
                break;
            case 1: //go to the permissions activity
                dest = PopularAppsActivity.class;
                break;
            case 2: //go to the apps to avoid activity
                dest = TextActivity.class;
                fragment_id = FragmentIds.avoid_apps_fragment_id;
                break;
            default:    //something went wrong
                Log.e("Privacy Helper", "Error with Recycler view action handler");
                dest = null;
        }

        //If destination to go to
        if (dest != null) {
            if (fragment_id != null)
                startActivity(new Intent(this, dest).putExtra(getString(R.string.text_activity_intent_key), fragment_id));
            else
                startActivity(new Intent(this, dest));
        }
    }
}
