package org.privacyhelper.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.privacyhelper.R;

/**
 * OtherResourcesActivity
 * Class for listing out useful websites and appstores
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class OtherResourcesActivity extends AppCompatActivity implements View.OnClickListener {

    //Declare and intialise URL strings for buttons
    private static final String privacytools = "https://www.privacytools.io/";
    private static final String fdroid = "https://f-droid.org/";
    private static final String ddgBlog = "https://spreadprivacy.com/";
    private static final String fossDroid = "https://fossdroid.com/";
    private static final String youtubevid = "https://www.youtube.com/watch?v=6vNxslcf9AE";
    private static final String prismbreak = "https://prism-break.org/";
    private static final String pwoss = "https://pwoss.xyz/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_resources);

        //Initialize button variables
        Button ptoolsButton = (Button) findViewById(R.id.privacyToolsButton);
        Button fDroidButton = (Button) findViewById(R.id.fDroidButton);
        Button ddgButton = (Button) findViewById(R.id.ddgButton);
        Button fossDroidButton = (Button) findViewById(R.id.fossDroidButton);
        Button youtubeButton = (Button) findViewById(R.id.youtubeButton);
        Button prismBreakButton = (Button) findViewById(R.id.prismbreakButton);
        Button pwossButton = (Button) findViewById(R.id.pwossButton);

        //Set action handlers to this class/activity
        ptoolsButton.setOnClickListener(this);
        fDroidButton.setOnClickListener(this);
        ddgButton.setOnClickListener(this);
        fossDroidButton.setOnClickListener(this);
        youtubeButton.setOnClickListener(this);
        prismBreakButton.setOnClickListener(this);
        pwossButton.setOnClickListener(this);

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            /*Set the home action icon to home icon*/
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_home);
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        // Get the view id
        int id = v.getId();

        // Declare and initialize view intent
        Intent intent = new Intent(Intent.ACTION_VIEW);
        // Needed to keep track of url to go to
        String url;

        // Determine what link to go to
        switch(id) {
            case R.id.privacyToolsButton:   //privacy tools button selected
                url = privacytools;
                break;
            case R.id.fDroidButton: //fdroid button selected
                url = fdroid;
                break;
            case R.id.ddgButton:    //duckduckgo button selected
                url = ddgBlog;
                break;
            case R.id.fossDroidButton:  //fossdroid button selected
                url = fossDroid;
                break;
            case R.id.youtubeButton:
                url = youtubevid;
                break;
            case R.id.prismbreakButton:
                url = prismbreak;
                break;
            case R.id.pwossButton:  //pwoss button selected
                url = pwoss;
                break;
            default:    //something else
                url = null;
        }

        //If a link has been assigned
        if (url != null) {
            //Go to the link
            intent.setData(Uri.parse(url));
            startActivity(intent);
        }
    }
}
