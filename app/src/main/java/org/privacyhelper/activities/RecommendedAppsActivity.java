package org.privacyhelper.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.privacyhelper.R;

/**
 * RecommendedAppsActivity
 * Recommends open source apps and provides button to f-droid
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class RecommendedAppsActivity extends AppCompatActivity {

    //String URL to the fdroid store for button
    private static final String fDroidURL = "https://f-droid.org/en/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommended_apps);

        Button fDroidButton = (Button) findViewById(R.id.fDroidButton);

        fDroidButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open f-droid website in browser
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(fDroidURL));
                startActivity(intent);
            }
        });

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            /*Set the home action icon to home icon*/
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_home);
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
