package org.privacyhelper.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.preference.PreferenceManager;

import org.privacyhelper.R;
import org.privacyhelper.fragments.SettingsFragment;

public class SettingsActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    //Key for update frequency settings
    public static final String KEY_PREF_FREQUENCY_LIST = "frequency_preference";

    //Key for app theme settings
    public static final String KEY_THEME_PREF_LIST = "theme_preference";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment())
                .commit();

        //Assign on sharedpreference listener to this class
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            /*Set the home action icon to home icon*/
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_home);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //Register the shared pref listener
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Unregister the shared pref listener
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }
    @Override
    public boolean onSupportNavigateUp(){
        goToHome(); //refresh activity stack and go home
        finish();
        return true;
    }

    @Override
    public void onBackPressed(){
        //Override back button press to refresh activity stack and go home
        onSupportNavigateUp();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        //If key is the app theme key
        if (key.equals(KEY_THEME_PREF_LIST)) {
            //Grab the theme string
            String display_theme = sharedPreferences.getString(KEY_THEME_PREF_LIST, "");
            //Enumerate which theme was selected
            switch (display_theme) {
                case "Dark":   // Dark theme
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    break;
                default:    //Light theme
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    break;
            }
            //Recreate this activity to apply the changes
            recreate();
        }
    }

    /*Goes to the homeactivity (main activity)*/
    private void goToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        /*Clear the stack from any other activities and start with home activity*/
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
