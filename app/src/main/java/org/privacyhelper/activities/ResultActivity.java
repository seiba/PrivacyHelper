package org.privacyhelper.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.privacyhelper.BitmapDownscaler;
import org.privacyhelper.R;

/**
 * ResultActivity
 * Displays the results of the quiz and allows sharing of app
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class ResultActivity extends AppCompatActivity {

    //Variable to hold the quiz result
    private int result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        int errorVal = getResources().getInteger(R.integer.default_err_val);    //get the error value integer

        result = getIntent().getIntExtra(getString(R.string.quiz_result_intent), errorVal);   //grab the int extra (result)
        //Check if result was transmitted successfully
        if (result != errorVal) {   //if result is not error value
            saveSharedPreferences();    //attempt to save the value to shared preferences
        }
        else {  //result send error
            Log.e("QuizActivity", "Problem has occurred with intent extra int");
        }

        Button shareButton = (Button) findViewById(R.id.shareButton);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent(Intent.ACTION_SEND)
                    .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_intent_message))
                    .setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
            }
        });

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            /*Set the home action icon to home icon*/
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_home);
        }

    }

    /*Saves the quiz score into saved preferences*/
    private void saveSharedPreferences() {
            //Store the quiz result to sharedpreferences in private context mode
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.results_shared_pref), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(getString(R.string.saved_previous_score_key), result).apply();
            
            updateResult(); //update the results screen
    }

    /*Change the back to home behavior to actually go back to home activity*/
    @Override
    public boolean onSupportNavigateUp(){
        goToHome(); //Don't go to previous activity, go to home activity
        finish();
        return true;
    }

    /*Goes to the homeactivity (main activity)*/
    private void goToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        /*Clear the stack from any other activities and start with home activity*/
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        /*
        Override back button pressed to go straight to main home activity
         */
        onSupportNavigateUp();
    }

    /* Method to display the outcome of the quiz
     * Array of image resource ids needed to convert each to a bitmap
     *
     * Intellectual property belongs to their respectful owners
     * All rights reserved
     * */
    private void updateResult() {
        /*Quiz result and additional info strings*/
        final int[] results = {R.string.no_concerns_result_text, R.string.possible_concerns_result_text, R.string.serious_concerns_result_text};
        final int[] info = {R.string.no_concerns_action_text, R.string.possible_concerns_action_text, R.string.serious_concerns_action_text};

        int threshold = getResources().getInteger(R.integer.result_thres);    //defines the threshold value to determine quiz result

        //Array to hold all of the images
        final int[] imageIds = {R.drawable.green_tick, R.drawable.yellow_exclamation, R.drawable.red_cross};

        //Convert the images into smaller bitmaps
        Bitmap[] bitmaps = convertBitmaps(imageIds);

        //Get the textviews
        TextView resultText = findViewById(R.id.resultTextView);
        TextView actionText = findViewById(R.id.actionTextView);

        //Get the imageview
        ImageView imageView = findViewById(R.id.resultImageView);

        //Check how well user did in the quiz
        if (result >= threshold) {  //Did well
            resultText.setText(results[0]);
            actionText.setText(info[0]);
            imageView.setImageBitmap(bitmaps[0]);
        }
        else if (result <= -threshold) {    //Did bad
            resultText.setText(results[2]);
            actionText.setText(info[2]);
            imageView.setImageBitmap(bitmaps[2]);
        }
        else {  //Did alright
            resultText.setText(results[1]);
            actionText.setText(info[1]);
            imageView.setImageBitmap(bitmaps[1]);
        }
    }

    /*Converts the images from image id reference to a smaller bitmap image*/
    private Bitmap[] convertBitmaps(int[] imageIds) {
        //IMPORTANT: convert all image icons to bitmaps for much faster loading
        Bitmap[] bitmaps = new Bitmap[imageIds.length];
        //Get the required dimension for the app icon
        int dimen = getResources().getInteger(R.integer.result_icon_dimen);

        for (int i = 0; i < imageIds.length; i++) {
            bitmaps[i] = BitmapDownscaler.decodeSampledBitmapFromResource(getResources(), imageIds[i], dimen, dimen);
        }

        return bitmaps;
    }
}
