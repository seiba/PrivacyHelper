package org.privacyhelper.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import org.privacyhelper.R;

/**
 * AboutActivity
 * A class for displaying about information and licenses
 * @author Joshua Lay
 * @version 1
 * Modified: 18/04/18
 * Copyright (c) 2018
 */
public class AboutActivity extends AppCompatActivity {

    // String location of the attributions html document
    private static final String licensedoc = "file:///android_asset/attributions.html";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Button button = (Button) findViewById(R.id.legalButton);

        //Assign click listener to button
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayLicensesAlertDialog();
            }
        });

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            /*Set the home action icon to home icon*/
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_home);
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    /*Displays an android webview in a alert dialog
    * Original Author: Matt Compton
    * Link: https://www.bignerdranch.com/blog/open-source-licenses-and-android/
    * Modified by: Joshua Lay */
    private void displayLicensesAlertDialog() {
        try {
            //Create a new webview (don't need to show in the current activity)
            WebView view = new WebView(this);
            //Load url into the view
            view.loadUrl(licensedoc);
            //Show view in a dialog
            new AlertDialog.Builder(AboutActivity.this)
                    .setTitle(R.string.attributions_text)
                    .setView(view)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
        } catch (Exception e) {
            //Something went wrong (e.g. no webview)
            e.printStackTrace();
            Snackbar.make(findViewById(R.id.aboutConstraintLayout), R.string.license_info_error_text, Snackbar.LENGTH_LONG)
                    .show();
        }
    }
}
