package org.privacyhelper.activities;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.privacyhelper.R;

import java.util.ArrayList;

/**
 * QuizActivity
 * A simple yes/no type quiz to get an understanding on user's privacy skills
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class QuizActivity extends AppCompatActivity implements View.OnClickListener {

    //ArrayList to hold all of the questions
    private static final int[] questions = {R.string.experienced_android_question, R.string.permissions_installed_question,
                                        R.string.disable_sensors_question, R.string.check_reviews_question,
                                        R.string.system_updates_question, R.string.privacy_statements_question,
                                        R.string.kindof_data_question};

    //Declare class variables for widgets
    private TextView questionText;
    private ProgressBar progressBar;

    //define variables to keep track of user progress and score
    private int questionNum = 0;
    private int score = 0;
    private int sbcounter = 0;  //needed to prevent snackbar from reappearing

    //arraylist to hold all previous scores
    private ArrayList<Integer> previousScore = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        //initialize all element variables
        Button yesButton = (Button) findViewById(R.id.yesButton);
        Button noButton = (Button) findViewById(R.id.noButton);
        questionText = (TextView) findViewById(R.id.questionTextView);
        progressBar = (ProgressBar) findViewById(R.id.quizProgressBar);

        //set the progress bar colour
        setProgressColour();

        //just in-case the text has not been initialized properly
        updateQuestion();

        //set action handler to this activity
        yesButton.setOnClickListener(this);
        noButton.setOnClickListener(this);

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            /*Set the home action icon to home icon*/
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_home);
        }

    }

    /*Sets the progress bar colour to the accent colour*/
    private void setProgressColour() {
        //get the accent colour
        int colour = ResourcesCompat.getColor(getResources(), R.color.colorAccent, null);
        //set the colour of the progress bar programmatically
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {    //if lollipop or newer
            //use recommended method
            progressBar.setProgressTintList(ColorStateList.valueOf(colour));   //set to accent colour
        } else {    //older android version
            //use traditional way (color filter)
            Drawable progressDrawable = progressBar.getProgressDrawable().mutate();
            progressDrawable.setColorFilter(colour, PorterDuff.Mode.SRC_IN);    //src in is just one method, others exist as well
            progressBar.setProgressDrawable(progressDrawable);  //set to accent colour
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    /*Goes back to the previous question to restores previous results*/
    private void backQuestion() {
        if (questionNum > 0) {  //second question or later
            //Pop the last integer from the arraylist
            score = previousScore.remove(previousScore.size() - 1);
            questionNum--;
            updateQuestion();
        }
    }

    @Override
    public void onBackPressed() {
        /*
        Overrides the default back button pressed logic to go back question until first
         */
        if (questionNum <= 0) { //first question, nothing to back back to...
            onSupportNavigateUp();
        } else {    //not first question, question before to go back to
            backQuestion();
        }
    }

    //updates the score if required
    private void updateScore(int res) {
        switch (res) {
            case 0: //user selects no
                previousScore.add(score);
                score--;
                break;
            case 1: //user selects yes
                previousScore.add(score);
                score++;
                break;
            default:    //something not right
                Log.e("PrivacyHelper", "Quiz button handler error");
        }
    }

    //Method to move onto the next question
    private void updateQuestion() {
        //The maximum value for the progress bar
        int progressMax = 100;

        //Calculate the value to set the progress bar to
        int val = (int) ((questionNum/(double) questions.length)*progressMax);

        /*
        Sets the progress with animation
        Original link: https://stackoverflow.com/a/16002144
        Modified by: Joshua Lay
         */
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", val);
        animation.setDuration(500); //500ms
        animation.setInterpolator(new AccelerateDecelerateInterpolator());    //Use the accel/decel interpolator
        animation.start();  //actually start the animation

        //Show the snackbar help text if required
        showSnackbars();

        //Update the textviews and radiobutton texts
        questionText.setText(questions[questionNum]);
    }

    /*Shows the snackbar quiz help text*/
    private void showSnackbars() {
        String message; //the string message to show

        //Check question number
        if (questionNum == 0 && sbcounter == 0) {   //first quest and not displayed yet
            message = getString(R.string.first_question_sb_text);
            sbcounter++;
        } else if (questionNum == 1 && sbcounter == 1) { //second question and not displayed yet
            message = getString(R.string.quiz_mistake_sb_text);
            sbcounter++;
        } else {    //all other questions
            message = null;
        }

        //If message set
        if (message != null) {
            //Show snackbar message
            Snackbar.make(findViewById(R.id.quizConstraintView), message, Snackbar.LENGTH_LONG).show();
        }
    }

    /*Resets the quiz to the first question if required*/
    private void resetQuiz() {
        //Reset question num and score
        questionNum = 0;
        score = 0;
        //Update the question
        updateQuestion();
    }

    //Launches the results activity
    private void launchResults() {
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra(getString(R.string.quiz_result_intent), score);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId(); //id of the view item
        int val; //value to use to check positive or negative score

        if (id == R.id.yesButton || id == R.id.noButton) {  //if yes/no buttons selected
            switch (id) {   //1 = yes, 0 = no, -1 = something happened
                case R.id.yesButton:    //yes button selected
                    val = 1;
                    break;
                case R.id.noButton: //no button selected
                    val = 0;
                    break;
                default:    //something else
                    val = -1;
            }

            //update the score
            updateScore(val);

            //change question if required or go to results activity
            if (questionNum < (questions.length-1)) {   //if not last question
                questionNum++;
                updateQuestion();
            }
            else if (questionNum == (questions.length-1)) {   //if last question...
                launchResults();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_activity_quiz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_reset: //reset menu item selected
                resetQuiz();   //reset the quiz
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
