package org.privacyhelper.activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.privacyhelper.R;

/**
 * DisableUnusedActivity
 * Class for explaining how to disable unused apps
 * @author Joshua Lay
 * @version 1
 * Copyright (c) 2018
 */
public class DisableUnusedActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disable_unused);

        //assign click listener to this class
        Button listAppsButton = (Button) findViewById(R.id.listApplicationsButton2);
        listAppsButton.setOnClickListener(this);

        //Add support for back navigation action item
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        //Get the view id
        int id = v.getId();
        //If list applications button was clicked
        if (id == R.id.listApplicationsButton2) {
            //Attempt to open list of applications menu
            Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
            if (intent.resolveActivity(getPackageManager()) != null) {
                //List of applications menu present
                startActivity(intent);
            }
            else {
                //List of applications menu not found
                Snackbar.make(findViewById(R.id.permissionsMitigateConstraintLayout), R.string.error_list_apps_text, Snackbar.LENGTH_LONG)
                        .show();
            }
        }
    }
}
