# PrivacyHelper
Repository for the Privacy Helper Android App

<p align="center">
<a href="https://img.shields.io/badge/license-GPLv3-blue.svg"><img src="https://img.shields.io/badge/license-GPLv3-blue.svg"/></a>
</p>

![Home Activity](https://gitlab.com/seiba/PrivacyHelper/raw/pages/assets/home-activity.png "Privacy Helper Home Screen")
![Quiz Activity](https://gitlab.com/seiba/PrivacyHelper/raw/pages/assets/quiz-activity.png "Privacy Helper Quiz Screen")
![App list Activity](https://gitlab.com/seiba/PrivacyHelper/raw/pages/assets/applist-activity.png "Privacy Helper App list") 
![Permission Activity](https://gitlab.com/seiba/PrivacyHelper/raw/pages/assets/permissionlist-activity.png "Privacy Helper Permission list") 

## Official links

- [Website](https://seiba.gitlab.io/PrivacyHelper/) 
- [Changelog](https://gitlab.com/seiba/PrivacyHelper/blob/master/CHANGELOG.md)
- [Download from Google Play (recommended)](https://play.google.com/store/apps/details?id=org.privacyhelper)
- [Download from F-Droid (recommended)](https://f-droid.org/packages/org.privacyhelper/)
- [Download from Amazon Appstore (potentially outdated)](https://www.amazon.com/dp/B07GMYXCFG/ref=sr_1_1?s=mobile-apps&ie=UTF8&qid=1534765829&sr=1-1&keywords=privacy+helper)
- [Download from GitLab (testing only)](https://gitlab.com/seiba/PrivacyHelper/tags) 

## Introduction
Data privacy is one of the most hotly-discussed topics in the digital world today. Many developers have made an effort to help increase the security of the user's data, however code is not perfect. 

Currently, it is far too easy for users to enable permissions that will then potentially allow adversaries to uniquely identify the user and hence compromise their privacy. 

What Privacy Helper brings is a simple, easy-to-use educational app with that leverages the mobile platform to inform and demonstrate effectively and interactively. 

## Features
The following features are not finalized and could change throughout the development phase:
- Simple privacy quiz to test the user's privacy knowledge 
- Guide on data privacy, and the problem with Android 
- Guides on disabling permissions, unused apps, and other mitigations
- Simple map demonstration of location tracking
- List of *dangerous* permissions used by popular Android applications
- Guide on Open Source app alternatives 
- List of alternative resources to help the user increase their knowledge
- Sharing with popular platforms including Facebook, Twitter, and more

## Contents
- Java Classes
- Layout and Resource XML files
- App icons
- Attributions HTML document

All project files were created in the latest stable version of Android Studio. 

## Translations
There is a lot of information covered in this app, and so it may be tedious to translate all of the strings. If you want to help translate, do feel free to contact me and I'll arrange something

## Donate
I'll always keep my open-source projects free. However, if you ever feel the need to donate, here is the official donation link

### [Paypal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=TM27YUEP6KWW6)


## Third Party Sources
- osmdroid (OpenStreetMap)
- Google Support Libraries 
- Apptweak.io (for permissions.json file) 
- CircleImageView
- Logopedia (Android app icons)
- Wikimedia (Image assets)
- Stackoverflow
- Android studio

## Special Thanks
I'll like to give my special thanks to the Monash University Teaching Team for making this all possible and also PrivacyTools.io for providing an inspiration for ideas

## License 
![GNU GPLv3 logo](https://www.gnu.org/graphics/gplv3-127x51.png "GNU GPL v3 Logo")

Joshua Lay, 2018 (c). This project is copyrighted under the GPL license. 
