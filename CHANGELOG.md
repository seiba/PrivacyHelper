## 0.6-stable
- Simplify display theme to just light/dark modes
- Android P support
- Minor stability improvements

## 0.5-stable
- Downloading permissions now checks if internet connection is available 
- Added permission list auto update frequency setting 
- Added swipe to refresh to permission list 
- Search widget now shows correct hint 
- Bumped osmdroid version to 6.0.2 
- Map label now scale according to screen dpi 
- Added night/day themes (toggle in preferences) 
- Stability fixes and minor bug fixes elsewhere

## 0.4-stable
- Correct permission list is now displayed after filtering list of apps 
- List touch feedback has been improved (faster, more natural) 
- Several behind the scenes improvements for better maintainability and stability

## 0.3-stable
- Fixes sudden crash after selecting an app from the list
- App logo on main activity now uses a vector image for much improved clarity and sharpness
- Minor modifications all around for ease of maintainability for the future
 
## 0.2-stable
- Minor fixes to map location fixing and permissions checking
- List items now show full animation before jumping into next activity
- App list and permissions lists are now searchable
 
## 0.1-stable
- Made the app fully FOSS. No Google Play Service dependencies are present
- Various fixes to app behaviour and text
- No longer using x.y.z versioning
 
## 0.0.1-stable
- First release for PrivacyHelper
- Basic functionality such as Google Maps, Youtube Android Player, Quiz and sharing
